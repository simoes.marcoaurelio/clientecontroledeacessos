package br.com.clientecontroledeacessos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não cadastrado no sistema")
public class ClienteNaoCadastradoException extends RuntimeException{
}
