package br.com.clientecontroledeacessos.repositories;

import br.com.clientecontroledeacessos.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
