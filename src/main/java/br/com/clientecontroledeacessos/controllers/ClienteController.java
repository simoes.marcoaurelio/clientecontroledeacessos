package br.com.clientecontroledeacessos.controllers;

import br.com.clientecontroledeacessos.models.Cliente;
import br.com.clientecontroledeacessos.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente) {
        return clienteService.cadastrarCliente(cliente);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente consultarClientePorId(@PathVariable int id) {
        return clienteService.consultarClientePorId(id);
    }
}
