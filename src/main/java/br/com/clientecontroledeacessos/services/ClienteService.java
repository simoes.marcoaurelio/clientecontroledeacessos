package br.com.clientecontroledeacessos.services;

import br.com.clientecontroledeacessos.exceptions.ClienteNaoCadastradoException;
import br.com.clientecontroledeacessos.models.Cliente;
import br.com.clientecontroledeacessos.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if (!optionalCliente.isPresent()) {
            throw new ClienteNaoCadastradoException();
        }
        return optionalCliente.get();
    }
}
