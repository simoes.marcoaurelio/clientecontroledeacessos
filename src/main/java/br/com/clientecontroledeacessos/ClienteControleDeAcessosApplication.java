package br.com.clientecontroledeacessos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClienteControleDeAcessosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteControleDeAcessosApplication.class, args);
	}

}
